from django.conf.urls import url
from task import views

urlpatterns = [
    url(r'^$', views.MiloUsersList.as_view(), name='users'),
    url(r'^add/$', views.MiloUserCreate.as_view(), name='add_user'),
    url(r'^edit/(?P<pk>[0-9]+)/$', views.MiloUserUpdate.as_view(), name='edit_user'),
    url(r'^delete/(?P<pk>[0-9]+)/$', views.MiloUserDelete.as_view(), name='delete_user'),
    url(r'^(?P<pk>[0-9]+)/$', views.MiloUserDetails.as_view(), name='user_details'),
    url(r'^dowload/$', views.MiloUserDownload.as_view(), name='dowload_users_list'),
]
