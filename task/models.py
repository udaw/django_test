from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.timezone import now
from random import randint


class MiloUser(AbstractUser):
    date_of_birth = models.DateField(default=now,
                                                                            verbose_name='Birthday date'
                                                                            )
    random_int = models.IntegerField(default=randint(1,100),
                                                                            verbose_name='Random int',
                                                                            validators=[MinValueValidator(1), MaxValueValidator(100)]
                                                                            )

    def get_fields(self):
        return [(field.verbose_name, field._get_val_from_obj(self)) for field in MiloUser._meta.fields]
