import csv

from django.views.generic import View, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.http import HttpResponse
from django.urls import reverse

from models import MiloUser
from forms import AddUserForm, EditUserForm
from templatetags import task_extras


class CSVResponseMixin(object):
    csv_filename = 'csvfile.csv'

    def get_csv_filename(self):
        return self.csv_filename

    def render_to_csv(self, data):
        response = HttpResponse(content_type='text/csv')
        cd = 'attachment; filename="{0}"'.format(self.get_csv_filename())
        response['Content-Disposition'] = cd

        writer = csv.writer(response)
        for row in data:
            writer.writerow(row)

        return response


class MiloUserDownload(CSVResponseMixin, View):
    csv_filename = 'Users list.csv'
    users = MiloUser.objects.all()
    data = [[user.username, user.date_of_birth, user.random_int, 
                        task_extras.is_permited(user.date_of_birth),
                        task_extras.bizz_fuzz(user.random_int)] for user in users]

    def get(self, request, *args, **kwargs):
        return self.render_to_csv(self.data)


class MiloUsersList(ListView):
    model = MiloUser
    context_object_name = 'users'
    template_name = 'task/users_list.html'


class MiloUserDetails(DetailView):
    model = MiloUser
    context_object_name = 'user'
    template_name = 'task/user_details.html'


class MiloUserCreate(CreateView):
    model = MiloUser
    form_class = AddUserForm
    template_name = 'task/user_create_form.html'

    def get_success_url(self):
        return reverse('task:users')


class MiloUserUpdate(UpdateView):
    model = MiloUser
    form_class = EditUserForm
    template_name = 'task/user_edit_form.html'

    def get_success_url(self):
        return reverse('task:users')


class MiloUserDelete(DeleteView):
    model = MiloUser
    template_name = 'task/user_delete_form.html'

    def get_success_url(self):
        return reverse('task:users')
