from django.forms import ModelForm

from models import MiloUser


class AddUserForm(ModelForm):
    class Meta:
        model = MiloUser
        fields = ['username', 'first_name', 'last_name', 'password', 'date_of_birth']


class EditUserForm(ModelForm):
    class Meta:
        model = MiloUser
        fields = ['username', 'first_name', 'last_name', 'date_of_birth']
