```
#!
virtualenv test
cd test
git clone https://udaw@bitbucket.org/udaw/django_test.git
cd django_test
pip install requirements.txt
python manage.py migrate
python manage.py runserver

```